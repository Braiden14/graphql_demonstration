﻿using System;

namespace GraphqlDotNetSql.Model
{
    public class UserRegistration
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string EncryptedPassword { get; set; }
    }
}
