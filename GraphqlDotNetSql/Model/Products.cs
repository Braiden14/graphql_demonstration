﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphqlDotNetSql.Model
{
    [Table("Products")]
    public class Products
    {
        [Required] public Guid Id { get; set; }
        [Required] public Guid BrandId { get; set; }
        [Required, MaxLength(50)] public string Name { get; set; }
        [Required, MaxLength(100)] public string Description { get; set; }
        [Required] public int StockCount { get; set; }
        public double Price { get; set; }
        public ProductBrand ProductBrand { get; set; }
    }
}
