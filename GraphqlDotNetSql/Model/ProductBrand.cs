﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GraphqlDotNetSql.Model
{
    [Table("ProductBrand")]
    public class ProductBrand
    {
        [Required] public Guid Id { get; set; }
        [Required, MaxLength(50)] public string Name { get; set; }
    }
}
