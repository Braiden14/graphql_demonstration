﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphqlDotNetSql.Model;
using GraphqlDotNetSql.Repositories;
using HotChocolate;
using Microsoft.EntityFrameworkCore;

namespace GraphqlDotNetSql.Queries
{
    public class UserQueries
    {
        public async Task<List<User>> GetUsers([Service] MyDbContext dbContext) =>
            await dbContext
                .Users
                .AsNoTracking()
                .OrderBy(o => o.FirstName)
                .ToListAsync();
        
        public async Task<List<User>> GetUser([Service] MyDbContext dbContext, Guid id) =>
            await dbContext
                .Users
                .AsNoTracking()
                .Where(w => w.Id == id)
                .OrderBy(o => o.FirstName)
                .ToListAsync();
    }
}
