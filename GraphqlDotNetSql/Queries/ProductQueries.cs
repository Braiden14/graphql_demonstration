﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphqlDotNetSql.Model;
using GraphqlDotNetSql.Repositories;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;

namespace GraphqlDotNetSql.Queries
{
    public class ProductQueries
    {
        [UsePaging(IncludeTotalCount = true)]
        [UseSorting]
        [HotChocolate.Data.UseFiltering]
        public IQueryable<Products> GetProducts([Service] MyDbContext dbContext) =>
            dbContext.Product
                .Join(dbContext.ProductBrand, p => p.BrandId,
                    pb => pb.Id,
                    (p, pb) => new Products
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        BrandId = p.BrandId,
                        StockCount = p.StockCount,
                        ProductBrand = pb
                    }).AsQueryable();

        public async Task<List<Products>> GetProduct([Service] MyDbContext dbContext, Guid id) =>
            await dbContext
                .Product
                .AsNoTracking()
                .Include("Sale") // todo link sales to products 
                .Where(w => w.Id == id)
                .OrderBy(o => o.Name)
                .ToListAsync();
    }
}
