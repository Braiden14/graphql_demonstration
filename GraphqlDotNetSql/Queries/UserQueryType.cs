﻿using HotChocolate.Types;

namespace GraphqlDotNetSql.Queries
{
    public class UserQueryType : ObjectType<UserQueries>
    {
        protected override void Configure(IObjectTypeDescriptor<UserQueries> descriptor)
        {
            base.Configure(descriptor);

            descriptor
                .Field(f => f.GetUsers(default));

            descriptor
                .Field(f => f.GetUser(default, default))
                .Argument("id", a => a.Type<StringType>());
        }
    }
}
