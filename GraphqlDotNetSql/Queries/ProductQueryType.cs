﻿using GraphqlDotNetSql.Model;
using HotChocolate.Types;
using HotChocolate.Types.Pagination;

namespace GraphqlDotNetSql.Queries
{
    public class ProductQueryType : ObjectType<ProductQueries>
    {
        protected override void Configure(IObjectTypeDescriptor<ProductQueries> descriptor)
        {
            // base.Configure(descriptor);

            // descriptor
            //     .Field(f => f.GetProducts(default));

            // descriptor
            //     .Field(f => f.GetProduct(default, default))
            //     .Argument("id", a => a.Type<StringType>());
            // descriptor.Field(f => f.GetProductTest(default))
            // .UsePaging<ProductType>(options: new PagingOptions { IncludeTotalCount = true})
            // .UseSorting()
            // .UseFiltering();

            descriptor.Field(f => f.GetProducts(default));

            // HotChocolate.Data.us
        }
    }

    public class ProductType:ObjectType<Products>{
        protected override void Configure(IObjectTypeDescriptor<Products> descriptor){
            base.Configure(descriptor);
        }
    }
}
