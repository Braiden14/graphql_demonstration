﻿namespace GraphqlDotNetSql.Interfaces
{
    public interface IEncryptionService
    {
        string GetEncryptedPassword(string plainText);
    }
}
