﻿using System.Threading.Tasks;
using GraphqlDotNetSql.Model;

namespace GraphqlDotNetSql.Interfaces
{
    public interface IRegistrationService
    {
        Task<bool> Registration(UserRegistration userRegistration);
    }
}
