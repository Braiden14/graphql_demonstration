﻿using GraphqlDotNetSql.Model;
using Microsoft.EntityFrameworkCore;

namespace GraphqlDotNetSql.Repositories
{
    public class MyDbContext : DbContext
    {
        public static string DbConnectionString = "Data Source=localhost;Initial Catalog=FullStackDatabase;Integrated Security=True;";

        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        { }

        public DbSet<Products> Product { get; set; }
        public DbSet<ProductBrand> ProductBrand { get; set; }
        public DbSet<User> Users{ get; set; }

    }
}
