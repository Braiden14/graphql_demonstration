using FullStackApplication.Services;
using GraphqlDotNetSql.Interfaces;
using GraphqlDotNetSql.Mutations;
using GraphqlDotNetSql.Queries;
using GraphqlDotNetSql.Repositories;
using GraphqlDotNetSql.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using HotChocolate.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace GraphqlDotNetSql
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddTransient<IRegistrationService, RegistrationService>();
            // services.AddTransient<IEncryptionService, EncryptionService>();

            services.AddDbContext<MyDbContext>(options =>
                options.UseSqlServer(MyDbContext.DbConnectionString));

            services.AddControllers();
            services.AddGraphQLServer()
                .AddQueryType<ProductQueryType>()
                .AddFiltering()
                .AddSorting()
                .AddMutationType<ProductMutations>();
            // .AddQueryType<UserQueryType>()
            // .AddMutationType<UserMutations>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseWebSockets();
            // app.UseGraphQL();
            app.UseGraphiQL();
            app.UseRouting().UseEndpoints(endpoints => { endpoints.MapGraphQL("/graphql"); });
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    
}
