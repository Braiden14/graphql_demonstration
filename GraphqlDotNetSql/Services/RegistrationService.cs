﻿using System;
using System.Threading.Tasks;
using FullStackApplication.Services;
using GraphqlDotNetSql.Interfaces;
using GraphqlDotNetSql.Model;
using GraphqlDotNetSql.Repositories;

namespace GraphqlDotNetSql.Services
{
    public class RegistrationService : IRegistrationService
    {
        private readonly MyDbContext _dbContext;

        public RegistrationService(MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Registration(UserRegistration userRegistration)
        {
            var encryptedService = new EncryptionService();
            userRegistration.EncryptedPassword = encryptedService.GetEncryptedPassword(userRegistration.Password);


            var user = new User
            {
                FirstName = userRegistration.FirstName,
                Id = Guid.NewGuid(),
                Password = userRegistration.EncryptedPassword,
                Username = userRegistration.Username,
                Surname = userRegistration.Surname
            };

            await _dbContext.Users.AddAsync(user);
            return true;
        }
    }
}
