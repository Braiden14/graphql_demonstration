﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using GraphqlDotNetSql.Interfaces;

namespace FullStackApplication.Services
{
    public class EncryptionService : IEncryptionService
    {
        private const string SecurityKey = "BraidensKey";

        public string GetEncryptedPassword(string plainText)
        {
            var toEncryptedArray = Encoding.UTF8.GetBytes(plainText);

            var encryptionService = new MD5CryptoServiceProvider();
            var securityKeyArray = encryptionService.ComputeHash(Encoding.UTF8.GetBytes(SecurityKey));
            encryptionService.Clear();

            var cryptoService = new TripleDESCryptoServiceProvider
            {
                Key = securityKeyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            var encryption = cryptoService.CreateEncryptor();
            var resultArray = encryption.TransformFinalBlock(toEncryptedArray, 0, toEncryptedArray.Length);
            cryptoService.Clear();

            var sha = new SHA256Managed();
            
            resultArray = resultArray.Concat(new byte[] {0,1,3,5,3,2}).ToArray();


            var result = sha.ComputeHash(resultArray);

            return Convert.ToBase64String(result);
        }
    }
}
