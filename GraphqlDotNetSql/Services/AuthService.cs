﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using FullStackApplication.Services;
using GraphqlDotNetSql.Model;
using GraphqlDotNetSql.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace GraphqlDotNetSql.Services
{
    public class AuthService
    {
        private const string JwtSecret = "braiden_secret_Key";
        private const string JwtIssuer = "http://localhost:1234";
        private readonly MyDbContext _dbContext;

        public AuthService(MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public string Login(UserLogin userLogin)
        {
            var encryptionService = new EncryptionService();
            var encryptedPassword = encryptionService.GetEncryptedPassword(userLogin.Password);

            var userRegistration = _dbContext.Users
                .AsNoTracking()
                .FirstOrDefault(w => w.Username == userLogin.Username);

            if (userRegistration != null && userRegistration.Password != encryptedPassword)
            {
                throw new UnauthorizedAccessException("user login failed");
            }

            return GetToken(userLogin.Username);
        }

        private string GetToken(string username)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtSecret));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(JwtIssuer,
                JwtIssuer,
                GetClaims(username),
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            return jwtToken;
        }

        private IEnumerable<Claim> GetClaims(string username)
        {
            var claims = new List<Claim>
            {
                new Claim("username", username)
            };

            return claims;
        }

        public bool ValidateToken(string token)
        {
            var mySecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtSecret));

            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = JwtIssuer,
                    ValidAudience = JwtIssuer,
                    IssuerSigningKey = mySecurityKey
                }, out _);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
