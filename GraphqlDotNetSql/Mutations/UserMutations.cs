﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using GraphqlDotNetSql.Interfaces;
using GraphqlDotNetSql.Model;
using GraphqlDotNetSql.Repositories;
using HotChocolate;
using Microsoft.EntityFrameworkCore.Internal;

namespace GraphqlDotNetSql.Mutations
{
    public class UserMutations
    {
        private readonly MyDbContext _dbContext;
        private readonly IRegistrationService _registrationService;

        public UserMutations([Service]  MyDbContext dbContext, IRegistrationService registrationService)
        {
            _dbContext = dbContext;
            _registrationService = registrationService;
        }

        public async Task<Guid> AddNewUser([Required] UserRegistration user)
        {
            user.Id = Guid.NewGuid();
            await _registrationService.Registration(user);
            return user.Id;
        }

        public Guid UpdateUser([Required] User user)
        {
            _dbContext.Users.Update(user);
            return user.Id;
        }

        public Guid DeleteUser([Required] User user)
        {
            _dbContext.Users.Remove(user);
            return user.Id;
        }
    }
}
