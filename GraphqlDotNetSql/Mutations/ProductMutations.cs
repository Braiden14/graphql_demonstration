﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GraphqlDotNetSql.Model;
using GraphqlDotNetSql.Repositories;
using HotChocolate;

namespace GraphqlDotNetSql.Mutations
{
    public class ProductMutations
    {
        private readonly MyDbContext _dbContext;

        public ProductMutations([Service]  MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Guid> AddNewProduct([Required] Products product)
        {
            await _dbContext.Product.AddAsync(product);
            return product.Id;
        }

        public Guid UpdateProduct([Required] Products product)
        {
            _dbContext.Product.Update(product);
            return product.Id;
        }

        public Guid DeleteProduct([Required] Products product)
        {
            _dbContext.Product.Remove(product);
            return product.Id;
        }
    }
}
